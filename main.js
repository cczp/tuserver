#!/usr/bin/env node

var
	crypto = require("crypto"),
	path = require("path"),
	fs = require("fs"),
	readline = require("readline");

process.chdir(path.dirname(require.main.filename));

app = {
	//Secondary variables
		sessions: {},
		sessionStatuses: {},
		
		readlineInterface: readline.createInterface({
			input: process.stdin,
			output: process.stdout,
			
			prompt: ""
		}),
		
		socket: require("./socket.js"),
		virtualClient: require("./virtualClient.js"),
	
	//Methods
		run: function() {
			console.log("Starting server...");
				process.stdout.write("Reading configs... ");
					try {
						this.manifest = require("./package.json");
						this.config = require("./server.json");
					} catch (error) {
						console.log("\x1b[1m\x1b[31mfailed\x1b[0m");
						
						process.exit(1);
					}
				console.log("\x1b[1m\x1b[32mdone\x1b[0m");
				
				process.stdout.write("Adding events... ");
					process.on("uncaughtException", function(error) {
						try {
							console.log("Server crashed\n\n" + error.stack);
							app.suicide();
						} catch (error) {
							process.exit(1);
						}
					});
					this.readlineInterface.on("SIGINT", function() {
						app.suicide();
					});
					
					this.socket.on("info", function(connection) {
						connection.reply("info", app.config.info);
					});
					
					this.socket.on("login", function(connection, data) {
						if (Object.keys(app.sessions).length >= app.config.maxPlayers) {
							connection.reply("alert", {
								message: String(new ReferenceError("server is full"))
							}, true);
							
							return;
						}
						
						if (
							!data.login ||
							
							!(typeof data.login == "string") &&
							!(data.login instanceof String) ||
							
							String(data.login).length > 32
						) {
							connection.reply("alert", {
								message: String(new ReferenceError("invalid login"))
							}, true);
							
							return;
						}
						if (
							!(typeof data.password == "string") &&
							!(data.password instanceof String) ||
							
							String(data.password).length > 88
						) {
							connection.reply("alert", {
								message: String(new ReferenceError("invalid password"))
							}, true);
							
							return;
						}
						
						if (data.login in app.sessions) {
							connection.reply("alert", {
								message: String(new ReferenceError("this player already playing"))
							}, true);
							
							return;
						}
						
						//Check password
							var hash = crypto.createHash(
								"sha512"
							).update(data.password).digest("base64");
							
							if (data.login in game.config.list.passwords) {
								if (game.config.list.passwords[data.login] != hash) {
									connection.reply("alert", {
										message: String(new ReferenceError("wrong password"))
									}, true);
									
									return;
								}
							} else {
								game.config.list.passwords[data.login] = hash;
							}
						
						connection.data.login = data.login;
						
						app.sessions[data.login] = connection;
						
						app.virtualClient.players.connect(data.login);
					});
					
					this.socket.on("input", function(connection, data) {
						if (!(connection.data.login in app.sessions)) {
							connection.reply("alert", {
								message: String(new ReferenceError("player not logged in"))
							}, true);
							
							return;
						}
						
						app.virtualClient.handleInput(connection.data.login, data);
					});
					
					this.socket.on("alert", function(connection, data) {
						if (!(connection.data.login in app.sessions)) {
							connection.reply("alert", {
								message: String(new ReferenceError("player not logged in"))
							}, true);
							
							return;
						}
						
						app.virtualClient.handleDialog(
							connection.data.login,
						"alert", data.result);
					});
					this.socket.on("prompt", function(connection, data) {
						if (!(connection.data.login in app.sessions)) {
							connection.reply("alert", {
								message: String(new ReferenceError("player not logged in"))
							}, true);
							
							return;
						}
						
						app.virtualClient.handleDialog(
							connection.data.login,
						"prompt", data.result);
					});
					this.socket.on("confirm", function(connection, data) {
						if (!(connection.data.login in app.sessions)) {
							connection.reply("alert", {
								message: String(new ReferenceError("player not logged in"))
							}, true);
							
							return;
						}
						
						app.virtualClient.handleDialog(
							connection.data.login,
						"confirm", data.result);
					});
				console.log("\x1b[1m\x1b[32mdone\x1b[0m");
				
				process.stdout.write("Initializing virtual client... ");
					this.virtualClient.init();
				console.log("\x1b[1m\x1b[32mdone\x1b[0m");
				
				this.socket.run();
		},
		
		suicide: function() {
			console.log("Shutting server down...");
				process.stdout.write("Kicking all players... ");
					for (var i in this.sessions) {
						try {
							this.sessions[i].terminate();
						} catch (error) {}
					}
				console.log("\x1b[1m\x1b[32mdone\x1b[0m");
				
				process.stdout.write("Autosaving... ");
					game.config.write("gameWorld");
					game.config.write("passwords");
					game.config.write("players");
				console.log("\x1b[1m\x1b[32mdone\x1b[0m");
			
			process.exit(0);
		}
};

app.run();
