var
	fs = require("fs"),
	https = require("https"),
	ws = require("ws");

module.exports = {
	//Secondary variables
		connections: [],
		
		eventHandlers: {},
	
	//Methods
		run: function(host, port) {
			try {
				this.httpsServer = https.createServer({
					key: fs.readFileSync('key.pem'),
					cert: fs.readFileSync('cert.pem')
				});
				
				this.httpsServer.listen({
					host: app.config.host,
					port: app.config.port
				}, function() {
					console.log(
						"\x1b[1m\x1b[32mServer started on " +
						module.exports.server.address().address + ":" +
						module.exports.server.address().port +
						"\x1b[0m"
					);
				});
				
				this.server = new ws.Server({
					server: this.httpsServer
				});
				
				this.server.on("connection", this.connectionHandler);
			} catch (error) {
				throw new ReferenceError(
					"SSL key/certificate is invalid or something is dead " +
					
					"(" + error + ")"
				);
			}
		},
		
		connectionHandler: function(connection) {
			connection.reply = module.exports.connection_reply;
			
			if (module.exports.server.clients.size >= app.config.maxConnections) {
				connection.reply("alert", {
					message: String(new ReferenceError("server is overloaded"))
				}, true);
				
				return;
			}
			
			connection.data = {
				chunk: "",
				
				alert: {},
				prompt: {},
				confirm: {}
			};
			
			connection.on("message", function(rawMessage) {
				var message = null;
				try {
					message = JSON.parse(rawMessage);
				} catch (error) {}
				
				if (message) {
					if ("event" in message) {
						if (message.event in module.exports.eventHandlers) {
							module.exports.eventHandlers[message.event](connection, message);
							
							return;
						}
					}
				}
				
				connection.reply("alert", {
					message: String(new TypeError("client sent invalid packet"))
				}, true);
			});
			
			connection.once("close", function() {
				delete app.sessions[connection.data.login || ""];
				
				app.virtualClient.players.disconnect(connection.data.login);
			});
			connection.on("error", function() {});
		},
		
		connection_reply: function(event, data, closeConnection) {
			if (!data) {
				data = {};
			}
			
			if (event) {
				data.event = event;
			}
			
			this.send(JSON.stringify(data), [
				function() {},
				(function() {
					this.terminate();
				}).bind(this)
			][+!!closeConnection]);
		},
		
		on: function(event, handler) {
			this.eventHandlers[event] = handler;
		},
		emit: function(connection, event, data) {
			this.eventHandlers[event](connection, data);
		}
};
