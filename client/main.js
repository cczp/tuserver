game = {
	screen: {
		width: 320,
		height: 240,
		
		multiplierX: 3,
		multiplierY: 3
	},
	
	debugInfo: [],
	
	textures: {},
	sounds: {},
	
	locales: {},
	get currentLocale() {
		return this.locales[
			this.config.list.main.locale
		] || this.locales[
			this.config.defaultList.main.locale
		];
	},
	
	realms: {},
	get currentRealm() {
		return this.realms.gameWorld;
	},
	set currentRealm(value) {},
	
	socket: {
		reply: function() {},
		
		on: function() {},
		emit: function() {}
	}
};

alert = function(message) {
	return new Promise(function(resolve, reject) {
		try {
			app.sessions[
				app.virtualClient.players.current
			].reply("alert", {
				message: message
			});
			
			app.sessions[
				app.virtualClient.players.current
			].data.alert.handler = resolve;
		} catch (error) {
			console.log(message);
		}
	});
};
prompt = function(message, defaultText) {
	return new Promise(function(resolve, reject) {
		try {
			app.sessions[
				app.virtualClient.players.current
			].reply("prompt", {
				message: [
					message,
					defaultText
				]
			});
			
			app.sessions[
				app.virtualClient.players.current
			].data.prompt.handler = resolve;
		} catch (error) {
			console.log(message);
		}
	});
};
confirm = function(message) {
	return new Promise(function(resolve, reject) {
		try {
			app.sessions[
				app.virtualClient.players.current
			].reply("confirm", {
				message: message
			});
			
			app.sessions[
				app.virtualClient.players.current
			].data.confirm.handler = resolve;
		} catch (error) {
			console.log(message);
		}
	});
};
