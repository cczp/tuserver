Object.defineProperty(
	game.realms.gameWorld.guiTypes.pause, "playerList",
	
	{
		configurable: true,
		enumerable: true,
		
		get: function() {
			return Object.keys(app.virtualClient.players.list);
		}
	}
);

game.realms.gameWorld.guiTypes.pause.controls = [
	{
		type: "button",
		
		get caption() {
			return game.currentLocale.gui_gameWorld_pause_resume;
		},
		
		x: 60,
		y: 82,
		
		event: function() {
			game.realms.gameWorld.currentGuiType = game.realms.gameWorld.guiTypes.default;
		}
	},
	
	{
		type: "button",
		
		get caption() {
			return game.currentLocale.gui_gameWorld_pause_info;
		},
		
		x: 60,
		y: 110,
		
		event: function() {
			alert(
				app.config.info.name + " at " +
				app.config.host + ":" + app.config.port
			);
		}
	},
	
	{
		type: "button",
		
		get caption() {
			return game.currentLocale.gui_gameWorld_pause_exit;
		},
		
		x: 60,
		y: 138,
		
		event: function() {
			app.sessions[Object.keys(
				app.virtualClient.players.list
			)[Object.values(
				app.virtualClient.players.list
			).indexOf(game.realms.gameWorld.playerObject)]].terminate();
		}
	}
];
