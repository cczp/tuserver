Object.defineProperty(
	game.realms.gameWorld, "currentGuiType",
	
	{
		configurable: true,
		enumerable: true,
		
		get: function() {
			return this.guiTypes[
				app.virtualClient.players.list[
					app.virtualClient.players.current 
				].currentGuiType
			];
		},
		set: function(value) {
			app.virtualClient.players.list[
				app.virtualClient.players.current 
			].currentGuiType = Object.keys(
				this.guiTypes
			)[Object.values(
				this.guiTypes
			).indexOf(value)];
		}
	}
);

game.realms.gameWorld.fail = function(playerObject) {
	var playerLogin = Object.keys(
		app.virtualClient.players.list
	)[Object.values(
		app.virtualClient.players.list
	).indexOf(playerObject)];
	
	game.realms.gameWorld.objects[[
		playerObject.position.x,
		playerObject.position.y
	]] = null;
	
	delete game.config.list.players[playerLogin];
	delete app.virtualClient.players.list[playerLogin];
	
	app.sessions[playerLogin].reply("sound", {
		sound: "entities_death"
	});
	
	app.sessions[playerLogin].reply("alert", {
		message: "You died"
	}, true);
	
	console.log(playerLogin + " died");
	game.realms.gameWorld.guiTypes.default.writeToChat("",
		playerLogin + game.currentLocale.gui_multiplayerWorld_died,
	game.colorScheme.text_bad);
};
