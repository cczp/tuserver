game.mods = {
	//Secondary variables
		list: {},
	
	//Methods
		load: function(name, version, description, foldersWithScripts) {
			if (name in this.list) {
				throw new ReferenceError("mod " + name + " already loaded");
			}
			
			version = version || "v1";
			description = description || "Just a mod";
			foldersWithScripts = foldersWithScripts || [];
			
			this.list[name] = {
				name: name,
				version: version,
				description: description,
				foldersWithScripts: foldersWithScripts
			};
			
			game.foldersWithScripts = game.foldersWithScripts.concat(foldersWithScripts);
		}
};
