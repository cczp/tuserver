game.config = {
	//Secondary variables
		list: {},
		defaultList: {
			players: {},
			passwords: {}
		},
		
		pathPrefix: "config/",
		paths: {
			main:
				"main.json.gz",
			gameWorld:
				"gameWorld.json.gz",
			passwords:
				"passwords.json.gz",
			players:
				"players.json.gz"
		},
		
	//Methods
		read: function() {
			this.list = JSON.parse(JSON.stringify(this.defaultList));
			
			for (var i in this.list) {
				var currentConfig = "";
				try {
					currentConfig = fs.readFileSync(
						this.pathPrefix +
						this.paths[i]
					);
				} catch (error) {}
				
				if (currentConfig) {
					try {
						this.list[i] = JSON.parse(zlib.gunzipSync(currentConfig));
					} catch (error) {
						alert(new ReferenceError(
							"can't read config " +
							this.pathPrefix +
							this.paths[i] +
							
							" (" + error + ")"
						));
					}
				}
			}
		},
		
		write: function(parameter) {
			try {
				fs.writeFileSync(
					this.pathPrefix +
					this.paths[parameter],
					
					zlib.gzipSync(JSON.stringify(this.list[parameter]))
				);
			} catch (error) {
				alert(new ReferenceError(
					"can't write config " +
					this.pathPrefix +
					this.paths[parameter] +
					
					" (" + error + ")"
				));
			}
		}
};
