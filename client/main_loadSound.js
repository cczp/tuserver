game.loadSound = function() {
	return {
		play: function() {
			app.sessions[
				app.virtualClient.players.current
			].reply("sound", {
				sound: this.sound
			});
		}
	};
};
