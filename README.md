# THIS PIECE OF SOFTWARE IS DEPRECATED

ToberUberStobe used to have multiplayer support, now it doesn't and will never do

Despite being in 7.\*.\* branch it doesn't work with the 7.\*.\* branch ToberUberStobe, but probably works with 6.\*.\* ones

---

tuserver is the reference ToberUberStobe miltiplayer server implementation

## How to run

1. Download [ToberUberStobe](https://gitlab.com/0x46d59b71/ToberUberStobe), version 6.0 or lower.\*
2. Extract it to the server folder\* as a folder named `Game`
3. Configure the server with the `server.json` file inside the server folder
4. Run the server `tuserver`

\*Locates in your node\_modules folder
